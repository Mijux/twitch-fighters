from PIL import Image
from os import listdir, mkdir
from os.path import isfile, join, exists


def main():
    directory = input("What is the directory ? > ")
    
    new_dir = join("./src/assets/formatted", directory)
    print(new_dir)
    if not exists(new_dir):
        mkdir(new_dir)

    image_in_dir = [f for f in listdir(directory) if isfile(join(directory, f))]

    for file in image_in_dir:
        columns, row = input(f"How many col and rows has {file} ? (x;y) > ").split(";")
        columns, row = int(columns), int(row)

        file_without_ex = file.split(".")[0]
        mkdir(join(new_dir,file_without_ex))

        with Image.open(join(directory, file)) as image:
            x, y = image.size

            sprite_size_y = int(y / row)
            sprite_size_x = int(x / columns)

            print(f"Sprites have {sprite_size_x}x{sprite_size_y} size")
            counter = 0
            for i in range(row):
                for j in range(columns):
                    left = j * sprite_size_x
                    top = i * sprite_size_y
                    right = (j + 1) * sprite_size_x
                    bottom = (i + 1) * sprite_size_y

                    file = file.split(".")[0]

                    cropped = image.crop((left, top, right, bottom))
                    cropped.save(join(new_dir,file,f"{counter}.png"))
                    counter+=1


if "__main__" == __name__:
    main()
