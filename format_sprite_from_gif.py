from PIL import Image
from os import listdir, mkdir
from os.path import isfile, join, exists


def main():
    directory = input("What is the directory ? > ")
    
    new_dir = join("./src/assets/formatted", directory)
    print(new_dir)
    if not exists(new_dir):
        mkdir(new_dir)

    image_in_dir = [f for f in listdir(directory) if isfile(join(directory, f))]

    for file in image_in_dir:
        file_without_ex = file.split(".")[0]
        mkdir(join(new_dir,file_without_ex))

        with Image.open(join(directory, file)) as image:
            x, y = image.size
            file = file.split(".")[0]
            
            print(f"Sprites have {x}x{y} size")
            
            for i in range(image.n_frames):
                image.seek(i)
                image.save(join(new_dir,file,f"{i}.png"))

if "__main__" == __name__:
    main()
