<p style="display: flex; justify-content: center">
  <img style="display: inline-block" alt="GitLab forks" src="https://img.shields.io/gitlab/forks/Mijux/twitch-fighters?style=flat-square">
  ·
  <img style="display: inline-block" alt="GitLab stars" src="https://img.shields.io/gitlab/stars/mijux/twitch-fighters?color=EBEC34&style=flat-square">
  ·
  <img style="display: inline-block" alt="GitLab issues" src="https://img.shields.io/gitlab/issues/open/Mijux/twitch-fighters?style=flat-square">
  ·
  <img style="display: inline-block" alt="GitLab License" src="https://img.shields.io/gitlab/license/Mijux/twitch-fighters?style=flat-square">
</p>

<br />
<div align="center">
  <a href="https://gitlab.com/Mijux/twitch-fighters">
    <img src="https://gitlab.com/uploads/-/system/project/avatar/42194971/pp-tf.png" alt="Twitch Fighters" width="64" height="64">
  </a>

<h3 align="center">Twitch Fighters</h3>

  <p align="center">
Twitch Fighters is a game developed to allow viewers to have fun during a stream. Viewers use their channel points to purchase actions such as attacking and healing. The goal is to spend good time together !
    <br />
    <a href="https://gitlab.com/Mijux/twitch-fighters/-/wikis/home"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://github.com/github_username/repo_name">View Demo</a>
    ·
    <a href="https://gitlab.com/Mijux/twitch-fighters/-/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/Mijux/twitch-fighters/-/issues">Request Feature</a>
  </p>
</div>


## About The Project

Don't have so much time, any help would be nice :)

### Built With

* <img alt="JS" src="https://img.shields.io/badge/Javascript-Vanilla-%20%23f7df1e%20?style=flat-square&logo=javascript"/>
* <img alt="Python" src="https://img.shields.io/badge/Python-Easy%20Scripting-%23306998?style=flat-square&logo=python"/>
* <img alt="PixiJS" src="https://img.shields.io/badge/PixiJS-Manage Sprites-%23ec1b63?style=flat-square"/>


<!-- GETTING STARTED -->
## Getting Started

### Prerequisites

### Installation


<!-- USAGE EXAMPLES -->
## Usage

Use this space to show useful examples of how a project can be used. Additional screenshots, code examples and demos work well in this space. You may also link to more resources.

_For more examples, please refer to the [Documentation](https://example.com)_


<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement". 

Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request


<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE.txt` for more information.


<!-- CONTACT -->
## Contact

Mijux - [@MijuxDev](https://twitter.com/MijuxDev) - Don't hesitate to send me questions

Project Link: [https://gitlab.com/Mijux/twitch-fighters/](https://gitlab.com/Mijux/twitch-fighters/)