from PIL import Image
from os import listdir
from os.path import isfile, join

dir = input("Which directory ? > ")
BASIC_PATH = "./src/assets/formatted"

dir = join(BASIC_PATH, dir)

STATES = ["Attack", "Death", "Idle", "Run", "TakeHit"]

for state in STATES:
    sub_dir = join(dir,state)
    image_in_dir = [f for f in listdir(sub_dir) if isfile(join(sub_dir, f))]

    for image in image_in_dir:
        im = Image.open(join(sub_dir, image))

        x,y = im.size
        mul = int(100/y)
        
        im2 = im.resize((int(x*mul),int(y*mul)),Image.NEAREST)
        im2.save(join(sub_dir, image))
# Shows the image in image viewer