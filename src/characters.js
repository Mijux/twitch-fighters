const CHARACTERS = {
    "MartialHero2": {
        "stats": {
            "healthPoints" : 10,
            "attackDamage" : 3,
            "dodgeChance" : 0.3,
            "damageReduction" : 0.3
        }, 
        "attack": {
            "path": "./assets/formatted/MartialHero2/Attack",
            "frames": 4,
            "anchor": {
                "x": 0.5,
                "y": 0.64
            }
        },
        "death": {
            "path": "./assets/formatted/MartialHero2/Death",
            "frames": 7,
            "anchor": {
                "x": 0.5,
                "y": 0.64
            }
        },
        "idle": {
            "path": "./assets/formatted/MartialHero2/Idle",
            "frames": 4,
            "anchor": {
                "x": 0.5,
                "y": 0.64
            }
        },
        "run": {
            "path": "./assets/formatted/MartialHero2/Run",
            "frames": 8,
            "anchor": {
                "x": 0.5,
                "y": 0.64
            }
        },
        "th": {
            "path": "./assets/formatted/MartialHero2/TakeHit",
            "frames": 3,
            "anchor": {
                "x": 0.5,
                "y": 0.64
            }
        }
    },
    "Vagabond": {
        "stats": {
            "healthPoints" : 8,
            "attackDamage" : 4,
            "dodgeChance" : 0.7,
            "damageReduction" : 0
        }, 
        "attack": {
            "path": "./assets/formatted/Vagabond/Attack",
            "frames": 15,
            "anchor": {
                "x": 0.25,
                "y": 1
            }
        },
        "death": {
            "path": "./assets/formatted/Vagabond/Death",
            "frames": 7,
            "anchor": {
                "x": 0.5,
                "y": 1
            }
        },
        "idle": {
            "path": "./assets/formatted/Vagabond/Idle",
            "frames": 2,
            "anchor": {
                "x": 0.5,
                "y": 1
            }
        },
        "run": {
            "path": "./assets/formatted/Vagabond/Run",
            "frames": 8,
            "anchor": {
                "x": 0.5,
                "y": 1
            }
        },
        "th": {
            "path": "./assets/formatted/Vagabond/TakeHit",
            "frames": 4,
            "anchor": {
                "x": 0.5,
                "y": 1
            }
        }
    },
    "BlueWitch": {
        "stats": {
            "healthPoints" : 6,
            "attackDamage" : 6,
            "dodgeChance" : 0.6,
            "damageReduction" : 0
        }, 
        "attack": {
            "path": "./assets/formatted/BlueWitch/Attack",
            "frames": 9,
            "anchor": {
                "x": 0.25,
                "y": 0.90
            }
        },
        "death": {
            "path": "./assets/formatted/BlueWitch/Death",
            "frames": 12,
            "anchor": {
                "x": 0.5,
                "y": 0.90
            }
        },
        "idle": {
            "path": "./assets/formatted/BlueWitch/Idle",
            "frames": 5,
            "anchor": {
                "x": 0.5,
                "y": 0.90
            }
        },
        "run": {
            "path": "./assets/formatted/BlueWitch/Run",
            "frames": 8,
            "anchor": {
                "x": 0.5,
                "y": 0.90
            }
        },
        "th": {
            "path": "./assets/formatted/BlueWitch/TakeHit",
            "frames": 3,
            "anchor": {
                "x": 0.5,
                "y": 0.90
            }
        }
    },
    "Hobbit": {
        "stats": {
            "healthPoints" : 4,
            "attackDamage" : 4,
            "dodgeChance" : 0.8,
            "damageReduction" : 0
        }, 
        "attack": {
            "path": "./assets/formatted/Hobbit/Attack",
            "frames": 17,
            "anchor": {
                "x": 0.5,
                "y": 0.5
            }
        },
        "death": {
            "path": "./assets/formatted/Hobbit/Death",
            "frames": 12,
            "anchor": {
                "x": 0.5,
                "y": 0.5
            }
        },
        "idle": {
            "path": "./assets/formatted/Hobbit/Idle",
            "frames": 4,
            "anchor": {
                "x": 0.5,
                "y": 0.5
            }
        },
        "run": {
            "path": "./assets/formatted/Hobbit/Run",
            "frames": 10,
            "anchor": {
                "x": 0.5,
                "y": 0.5
            }
        },
        "th": {
            "path": "./assets/formatted/Hobbit/TakeHit",
            "frames": 4,
            "anchor": {
                "x": 0.5,
                "y": 0.5
            }
        }
    }
}