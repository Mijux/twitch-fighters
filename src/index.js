let app = new PIXI.Application({ width: 1250, height: 200, background: "#222222" });
document.getElementById("container").appendChild(app.view);


const ALL_SPRITES = {}

function loadAllAnimationFromCharacter(characterName) {
    let character = CHARACTERS[characterName]

    let complete_sprite = {}
    let anchor_anim = {}

    // ATTACK ANIMATION
    let attackLoadFrames = []
    for (let i = 0; i < character.attack.frames; i++) {
        let framePath = character.attack.path + `/${i}.png`;
        attackLoadFrames.push(framePath);
    }

    PIXI.Assets.load(attackLoadFrames).then(() => {
        let frames = []
        for (let i = 0; i < character.attack.frames; i++) {
            let framePath = character.attack.path + `/${i}.png`;
            frames.push(PIXI.Texture.from(framePath))
        }

        complete_sprite.attack = frames
        anchor_anim.attack = character.attack.anchor

        // DEATH ANIMATION
        let deathLoadFrames = []
        for (let i = 0; i < character.death.frames; i++) {
            let framePath = character.death.path + `/${i}.png`;
            deathLoadFrames.push(framePath);
        }

        PIXI.Assets.load(deathLoadFrames).then(() => {
            let frames = []
            for (let i = 0; i < character.death.frames; i++) {
                let framePath = character.death.path + `/${i}.png`;
                frames.push(PIXI.Texture.from(framePath))
            }

            complete_sprite.death = frames;
            anchor_anim.death = character.death.anchor


            // IDLE ANIMATION
            let idleLoadFrames = []
            for (let i = 0; i < character.idle.frames; i++) {
                let framePath = character.idle.path + `/${i}.png`;
                idleLoadFrames.push(framePath);
            }

            PIXI.Assets.load(idleLoadFrames).then(() => {
                let frames = []
                for (let i = 0; i < character.idle.frames; i++) {
                    let framePath = character.idle.path + `/${i}.png`;
                    frames.push(PIXI.Texture.from(framePath))
                }

                complete_sprite.idle = frames;
                anchor_anim.idle = character.idle.anchor

                // RUN ANIMATION

                let runLoadFrames = []
                for (let i = 0; i < character.run.frames; i++) {
                    let framePath = character.run.path + `/${i}.png`;
                    runLoadFrames.push(framePath);
                }

                PIXI.Assets.load(runLoadFrames).then(() => {
                    let frames = []
                    for (let i = 0; i < character.run.frames; i++) {
                        let framePath = character.run.path + `/${i}.png`;
                        frames.push(PIXI.Texture.from(framePath))
                    }

                    complete_sprite.run = frames
                    anchor_anim.run = character.run.anchor


                    // TAKE HIT ANIMATION
                    let thLoadFrames = []
                    for (let i = 0; i < character.th.frames; i++) {
                        let framePath = character.th.path + `/${i}.png`;
                        thLoadFrames.push(framePath);
                    }

                    PIXI.Assets.load(thLoadFrames).then(() => {
                        let frames = []
                        for (let i = 0; i < character.th.frames; i++) {
                            let framePath = character.th.path + `/${i}.png`;
                            frames.push(PIXI.Texture.from(framePath))
                        }

                        complete_sprite.th = frames;
                        anchor_anim.th = character.th.anchor

                        ALL_SPRITES[characterName] = {
                            "textures": complete_sprite,
                            "anchor": anchor_anim
                        };

                        newCharacter(characterName, playerName)
                    })
                })
            })
        })
    })
}

function addHealthbar(character, playerName) {
    const style = new PIXI.TextStyle({
        fontFamily: 'Arial',
        fontSize: 16,
        fill: ['#ffffff'],
    });

    let basicText = new PIXI.Text(playerName, style);
    basicText.x = character.x
    basicText.y = character.y - character.height / 2 - basicText.height + 40
    basicText.anchor.set(0.5)
    app.stage.addChild(basicText);
}

function newCharacter(characterName, playerName) {
    c = new Character(playerName, characterName, ALL_SPRITES[characterName].textures, ALL_SPRITES[characterName].anchor, CHARACTERS[characterName].stats)
    addHealthbar(c, playerName)
}

function addCharacter(characterName, playerName) {
    if (characterName) {
        if (app.stage.getChildByName(playerName) == undefined) {
            if (ALL_SPRITES[characterName] == undefined) {
                loadAllAnimationFromCharacter(characterName)
            } else {
                newCharacter(characterName, playerName)
            }
        } else {
            console.log("Player has already a character on the game");
        }
    }
}

function characterAction(action, playerName, ennemyPlayer) {
    switch (action) {
        case "attack":
            character = app.stage.getChildByName(playerName);
            if (character && ennemyPlayer) character.moveTo(app.stage.getChildByName(ennemyPlayer), "attack")
            break;
        case "death":
            character = app.stage.getChildByName(playerName);
            if (character) character.deathAnimation()
            break;
        case "idle":
            character = app.stage.getChildByName(playerName);
            if (character) character.idleAnimation()
            break;
        case "run":
            character = app.stage.getChildByName(playerName);
            if (character) character.runAnimation()
            break;
        case "takeHit":
            character = app.stage.getChildByName(playerName);
            if (character) character.takeHitAnimation()
            break;
        default:
            break;
    }
}


function gameLoop() {
    app.stage.children.forEach(children => {
        if (children.twitchName) {
            children.move()
        }
    });
    // let objToRemove = []
    // for (let i = 0; i < MOVING_ACTION.length; i++) {
    //     const ma = MOVING_ACTION[i];
    //     x_good = true;
    //     y_good = true;

    //     if (Math.abs(ma.A.x - ma.B.x) >= 50) {
    //         x_good = false
    //         if (ma.A.x < ma.B.x) ma.A.x = ma.A.x + CHARACTERS_VELOCITY
    //         else ma.A.x = ma.A.x - CHARACTERS_VELOCITY
    //     }
    //     if (Math.abs(ma.A.y - ma.B.y) >= CHARACTERS_VELOCITY) {
    //         y_good = false
    //         if (ma.A.y < ma.B.y) ma.A.y = ma.A.y + CHARACTERS_VELOCITY
    //         else ma.A.y = ma.A.y - CHARACTERS_VELOCITY
    //     }

    //     if (x_good && y_good) objToRemove.push(i);
    // }

    // while (objToRemove.length >= 1) {
    //     index = objToRemove.pop()
    //     let ma = MOVING_ACTION.splice(index, 1)[0];
    //     if (ma.action == "attack") ma.A.attack(ma.B)
    // }
}

app.ticker.add(gameLoop)

const style = new PIXI.TextStyle({
    fontFamily: 'Arial',
    fontSize: 16,
    fill: ['#ffffff'],
});