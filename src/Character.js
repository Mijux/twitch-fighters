const MOVING_ACTION = []

const X_CHARACTERS_VELOCITY = 5;
const Y_CHARACTERS_VELOCITY = 5;
class Character extends PIXI.AnimatedSprite {

    constructor(twitchName, characterName, allAnimationSprites, allAnimationAnchors, allStats) {
        super(allAnimationSprites.idle)


        // Primary Key
        this.twitchName = twitchName;
        this.name = twitchName;

        // Used to know which character is used
        this.characterName = characterName;

        // All animation textures
        this.allAnimationSprites = allAnimationSprites;
        this.allAnimationAnchors = allAnimationAnchors;

        // Basic Settings
        this.animationSpeed = 0.1
        this.anchor.set(this.allAnimationAnchors.idle.x, this.allAnimationAnchors.idle.y);

        this.x = Math.random() * (app.screen.width - 100) + 100;
        this.y = Math.random() * (app.screen.height - 100) + 100;
        console.log(this.x)
        console.log(this.y)

        this.play()

        app.stage.addChild(this)

        this.healthPoints = allStats.healthPoints;
        this.attackDamage = allStats.attackDamage;

        this.dodgeChance = allStats.dodgeChance;
        this.damageReduction = allStats.damageReduction;

        this.moving_action = null
    }


    attackAnimation() {
        this.loop = false;
        this.textures = this.allAnimationSprites.attack;
        this.anchor.set(this.allAnimationAnchors.attack.x, this.allAnimationAnchors.attack.y);
        this.play();

        this.onComplete = () => {
            this.idleAnimation()
        }
    }

    deathAnimation() {
        this.loop = false;
        this.anchor.set(this.allAnimationAnchors.death.x, this.allAnimationAnchors.death.y);
        this.textures = this.allAnimationSprites.death;
        this.play();

        this.onComplete = () => {
            app.stage.removeChild(this);
        }
    }

    idleAnimation() {
        this.loop = true;
        this.anchor.set(this.allAnimationAnchors.idle.x, this.allAnimationAnchors.idle.y);
        this.textures = this.allAnimationSprites.idle;
        this.play();
    }

    runAnimation() {
        this.loop = true;
        this.anchor.set(this.allAnimationAnchors.run.x, this.allAnimationAnchors.run.y);
        this.textures = this.allAnimationSprites.run;
        this.play();
    }

    takeHitAnimation() {
        this.loop = false;
        this.anchor.set(this.allAnimationAnchors.th.x, this.allAnimationAnchors.th.y);
        this.textures = this.allAnimationSprites.th;
        this.play();

        this.onComplete = () => {
            if (this.healthPoints > 0) this.idleAnimation();
            else this.deathAnimation();
        }
    }

    moveTo(destination, action) {
        this.moving_action = { "destination": destination, "action": action }
    }

    move() {
        const DISTANCE = 40;
        let isArrived = false;

        if (this.moving_action) { // If there is a action declared (moveTo function)
            let side = 1;
            if (this.x < this.moving_action.destination.x) side = -1;

            let xA = this.x;
            let yA = this.y;
            let xB_prim = this.moving_action.destination.x + side * DISTANCE;
            let yB = this.moving_action.destination.y;

            if (xA < xB_prim) {
                this.scale.x = 1;
            } else {
                this.scale.x = -1;
            }

            let len_x = Math.abs(xA - xB_prim)
            let len_y = Math.abs(yA - yB)

            if (Math.abs(xA - xB_prim) <= DISTANCE && Math.abs(yA - yB) < 1) isArrived = true;
            else {
                let ang;
                if (xA < xB_prim && yA < yB) {
                    ang = -Math.atan(len_y / len_x)
                } else if (xA > xB_prim && yA > yB) {
                    ang = Math.PI / 2 + Math.atan(len_x / len_y)
                } else if (xA < xB_prim && yA > yB) {
                    ang = Math.atan(len_y / len_x)
                } else if (xA > xB_prim && yA < yB) {
                    ang = -(Math.PI / 2 + Math.atan(len_x / len_y))
                } else if (xA == xB_prim) {
                    if (yA < yB) ang = -(Math.PI / 2)
                    else if (yA > yB) ang = Math.PI / 2;
                } else if (yA == yB) {
                    if (xA < xB_prim) ang = 0;
                    else if (xA > xB_prim) ang = Math.PI;
                }

                let xlocal_velocity = X_CHARACTERS_VELOCITY;
                let ylocal_velocity = Y_CHARACTERS_VELOCITY;

                if (len_x > 400) xlocal_velocity = xlocal_velocity * 1.5;
                else if (len_x > 150) xlocal_velocity = xlocal_velocity * 1.25;
                else if (len_x < DISTANCE + xlocal_velocity) xlocal_velocity = Math.abs(DISTANCE - len_x);

                if (Math.abs(xA - xB_prim) < 1) xlocal_velocity = 0;

                if (len_y > 300) ylocal_velocity = ylocal_velocity * 1.5;
                else if (len_y > 100) ylocal_velocity = ylocal_velocity * 1.25;
                else if (len_y < DISTANCE + ylocal_velocity) ylocal_velocity = Math.abs(DISTANCE - ylocal_velocity);


                this.x = this.x + Math.cos(ang) * xlocal_velocity
                this.y = this.y + Math.sin(ang + Math.PI) * ylocal_velocity
            }
        }
        if (isArrived) this.action()
    }

    action() {
        if (this.moving_action) {
            switch (this.moving_action.action) {
                case "attack":
                    if (!this.moving_action.destination) console.error("action : ERROR : no ennemie");
                    this.attack(this.moving_action.destination)
                    this.moving_action = null;
                    break;
                default:
                    console.error("This action can't be performed : " + this.moving_action.action);
                    break;
            }
        }
    }

    attack(ennemy) {
        let isAttackDodged = Math.random() < ennemy.dodgeChance ? true : false;
        console.log("Is Dodged ? " + isAttackDodged)

        if (!isAttackDodged) {
            let damage = this.attackDamage - (this.attackDamage * ennemy.damageReduction)

            console.log(damage);

            ennemy.healthPoints = ennemy.healthPoints - damage

            this.attackAnimation()

            setTimeout(() => {
                ennemy.takeHitAnimation()
            }, 1000)
        } else this.idleAnimation()
    }

}